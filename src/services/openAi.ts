import axios, { AxiosInstance } from "axios";
import { openAIapiKey } from "../utils/constants/constants";
import { Message } from "../components/ChatMessages";

const client: AxiosInstance = axios.create({
  headers: {
    "Authorization": "Bearer " + openAIapiKey,
    "Content-Type": "application/json"
  }
});

const chatGptEndpoint: string = 'https://api.openai.com/v1/chat/completions';


export const apiCall = async (prompt: string, messages?: Message[]): Promise<{ success: boolean; data?: Message[]; msg?: string; }> => {
  try {
    const res = await client.post(chatGptEndpoint, {
      model: 'gpt-3.5-turbo',
      messages: [{ role: "system", content: "Hello! How can I assist you?" },]
    });
    return chatGptCall(prompt, messages || []);
  } catch (error) {
    console.log("API ERROR:: ", error);
    return { success: false, msg: error.message };
  }
};

const chatGptCall = async (prompt: string, messages: Message[]): Promise<{ success: boolean; data?: Message[]; msg?: string; }> => {
  try {
    messages.length == 1 && messages.unshift({ role: "system", content: "You are an helpful assistant that communicates with older people. Be kind and say Hi the first time and Goodbye when the conversation is over. " })
    const res = await client.post(chatGptEndpoint, {
      model: 'gpt-3.5-turbo',
      messages: messages
    });

    let answer: string | undefined = res.data?.choices[0]?.message?.content;
    messages.push({ role: 'assistant', content: answer?.trim() || '' });
    return { success: true, data: messages };
  } catch (error) {
    console.log("API ERROR:: ", error);
    return { success: false, msg: error.message };
  }
};
