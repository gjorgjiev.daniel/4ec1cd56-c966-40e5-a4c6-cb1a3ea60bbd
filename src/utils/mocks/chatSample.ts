export const chatSample: { role: 'user' | 'assistant'; content: string }[] = [
    {
      role: 'user',
      content: 'How are you',
    },
    {
      role: 'assistant',
      content: 'I m fine and you?',
    },
    {
      role: 'user',
      content: 'Me also',
    },
    {
      role: 'assistant',
      content: 'Good',
    },
  ];