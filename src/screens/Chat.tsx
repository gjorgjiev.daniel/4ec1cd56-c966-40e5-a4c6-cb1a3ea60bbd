
import * as React from 'react';
import { View } from 'react-native';
import { apiCall } from '../services/openAi';
import RecordingButton from '../components/RecordingButton';
import CustomTextInput from '../components/CustomTextInput';
import ChatMessages, { Message } from '../components/ChatMessages';

export function Chat() {

    const [ messages, setMessages ] = React.useState<Message[]>([]) //List of messages that will be displayed as a chat. User and Assistant will be displayed with different styles.
    const [ gptQuery, setGptQuery ] = React.useState<String>('') //Recording or Input text to send as query for chatGPT.

    const ScrollViewRef = React.useRef()

    /**
     * Fetches GPT response from the API based on the current query.
     * @returns A promise that resolves when the API call is successful, and rejects if there is an error or the query is empty.
     */
    const fetchGptResponse = (): Promise<void> => {
        return new Promise<void>((resolve, reject) => {
          if (gptQuery.trim().length > 0) {
            let newMessages = [...messages];
            newMessages.push({ role: 'user', content: gptQuery.trim() });
            setMessages(newMessages);
            updateScroll();
      
            apiCall(gptQuery.trim(), newMessages)
              .then((res) => {
                console.log('got api data', res);
                if (res.success) {
                  setMessages(res.data);
                  updateScroll();
                  setGptQuery('');
                  resolve();
                } else {
                  console.log("Error in API call");
                  reject("Error in API call");
                }
              })
              .catch((error) => {
                console.error("Error in API call:", error);
                reject("Error in API call");
              });
          } else {
            reject("Empty query");
          }
        });
      };
      

    /**
     * Scrolls to the end of the ScrollView after a delay of 1000 milliseconds (after gpt response)
     * It is neccessary to show the last message
     */
    const updateScroll = () => {
        setTimeout(() => {
            ScrollViewRef?.current?.scrollToEnd({animated: true})
        }, 1000)
    }


  return (
    <View className="flex-1 px-4 justify-between">
        <View style={{height: "80%"}} className="space-y-2 flex">
            <ChatMessages messages={messages} scrollRef={ScrollViewRef} />
        </View>
        
        <View style={{height: "20%"}} className="flex justify-center items-center flex-row">
        <CustomTextInput
            setGptQuery={setGptQuery}
            onSubmitCallback={fetchGptResponse}
        />
        <RecordingButton
            onRecordFinishCallback={fetchGptResponse}
            setGptQuery={setGptQuery}
        />       
        </View>
    </View>
  );
}