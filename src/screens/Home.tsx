
import * as React from 'react';
import { View, Text, Button } from 'react-native';

export function Home({navigation}) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>No conversations here</Text>
      <Button
        title="Start a new conversation"
        onPress={() => navigation.navigate('Chat')}
      />
    </View>
  );
}