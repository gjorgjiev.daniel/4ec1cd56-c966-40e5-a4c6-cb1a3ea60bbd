import React, { useState } from 'react';
import { View, TextInput } from 'react-native';

type OnSubmitCallback = () => Promise<void>;

interface CustomTextInputProps {
  onSubmitCallback: OnSubmitCallback;
  setGptQuery: React.Dispatch<React.SetStateAction<string>>;
}

const CustomTextInput: React.FC<CustomTextInputProps> = ({ onSubmitCallback, setGptQuery }) => {
    const [text, setText] = useState<string>('');
  
    const handleInputChange = (inputText: string) => {
      setText(inputText);
      setGptQuery(inputText);
    };

  /**
 * Handles the submission of input text.
 * Logs the submitted text to the console.
 * Calls the onSubmitCallback function and clears the text input after the callback resolves.
 */
    const onInputTextSubmit = () => {
      console.log("Submitted text:", text);
      onSubmitCallback().then(() => {
        setText("");
      })
    };

  return (
    <View style={{width: "80%"}}>
    <TextInput 
        placeholder="Ask me something.." 
        onChangeText={handleInputChange}
        value={text}
        onSubmitEditing={onInputTextSubmit}
        className="border border-gray-300 rounded-lg text-base p-4 min-h-12 w-full"
    />
</View >
  );
};

export default CustomTextInput;
