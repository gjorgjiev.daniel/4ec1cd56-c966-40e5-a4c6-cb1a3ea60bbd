import React, { useState } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import Voice from '@wdragon/react-native-voice';

type OnRecordFinishCallback = () => Promise<void>;

interface RecordinButtonProps {
    onRecordFinishCallback: OnRecordFinishCallback;
    setGptQuery: React.Dispatch<React.SetStateAction<string>>;
  }

const RecordingButton: React.FC<RecordinButtonProps> = ({ onRecordFinishCallback, setGptQuery }) => {
    const [ recording, setRecording ] = React.useState<Boolean>(false)

    /**
     * VOICE TO TEXT HANDLERS
     */
    const onSpeechStartHandler = (e: { value: any[]; }) => {
        console.log("Speech start")
    }
    const onSpeechEndHandler = (e: { value: any[]; }) => {
        setRecording(false)
        console.log("Speech end")
    }
    const onSpeechResultHandler = (e: { value: any[]; }) => {
        console.log("Speech result: ", e)
        const text = e.value.join(" ")
        setGptQuery(text)
    }
    const onSpeechErrorHandler = (e: { value: any[]; }) => {
        console.log("Speech error: ", e)
    }
/**
 * Starts the recording process.
 * Sets the recording state to true and initiates voice recording using the 'en-US' language as default.
 * Logs an error message if there is an error during the recording start process.
 */
    const startRecording = async () => {
        setRecording(true)
        try {
            await Voice.start('en-US')
        } catch (error) {
            console.log("Erorr on start recording:: ", error)
        }
    }

    /**
 * Stops the recording process.
 * Stops voice recording.
 * Sets the recording state to false.
 * Calls the onRecordFinishCallback function to handle post-recording actions.
 * Logs an error message if there is an error during the recording stop process.
 */
    const stopRecording = async () => {
        try {
            await Voice.stop()
            setRecording(false)
            onRecordFinishCallback()
        } catch (error) {
            console.log("Erorr on start recording:: ", error)
        }
    }

    React.useEffect(()=> {
        Voice.onSpeechStart = onSpeechStartHandler;
        Voice.onSpeechEnd = onSpeechEndHandler;
        Voice.onSpeechResults = onSpeechResultHandler;
        Voice.onSpeechError = onSpeechErrorHandler;

        return () => {
            Voice.destroy().then(Voice.removeAllListeners);
        }
    }, [])

  return (
    <View className="justify-center items-center" style={{width: "20%"}}>
        {
            recording ? (
                <TouchableOpacity onPress={stopRecording}>
                    <Image 
                        className="rounded-full"
                        source={require("../assets/images/mic_animated.gif")}
                        style={{width: 45, height: 45}}/>
                </TouchableOpacity>
                ) : (
                <TouchableOpacity onPress={startRecording}>
                    <Image
                        className="rounded-full"
                        source={require("../assets/images/mic.png")}
                        style={{width: 25, height: 25}}>
                    </Image>
                </TouchableOpacity>
                )
        }
        </View>
  );
};

export default RecordingButton;
