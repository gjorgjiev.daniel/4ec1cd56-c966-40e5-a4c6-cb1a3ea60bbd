import React from 'react';
import { ScrollView, View, Text } from 'react-native';

export interface Message {
  role: 'assistant' | 'user';
  content: string;
}

interface Props {
  messages: Message[];
}

const ChatMessages: React.FC<Props> = ({ messages, scrollRef }) => {
  return (
    <>
      {messages.length > 0 ? (
        <ScrollView
          ref={scrollRef}
          bounces={false}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ flexGrow: 1 }}
        >
          {messages.map((message, index) => {
            switch (message.role) {
              case 'assistant':
                return (
                  <View key={index}style={{width: "80%"}} className=" bg-sky-300 rounded-xl p-2 my-1 rounded-tl-none">
                    <Text>{message.content}</Text>
                  </View>
                );
              case 'user':
                return (
                  <View key={index} className="flex-row justify-end" >
                    <View className="bg-emerald-100 rounded-xl p-2 my-1 rounded-br-none" style={{width: "80%"}}>
                        <Text>{message.content}</Text>
                    </View>
                  </View>
                );
              default:
                return null;
            }
          })}
        </ScrollView>
      ) : (
        <View className="flex-1 items-center justify-center text-center px-5">
          <Text className="text-center" style={{height: "80%"}}>Ask something by typing into the text box or use your voice</Text>
        </View>
      )}
    </>
  );
};

export default ChatMessages;
