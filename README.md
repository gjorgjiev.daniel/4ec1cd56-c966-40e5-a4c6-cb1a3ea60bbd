
# SS Demo Project 

Chat GPT assistant for elder people.
This two-page application is designed to provide users with a streamlined experience in asking a receiving answers from an assistant that is based on ChatGPT.

The assistant is able to answer more questions and the user has two possibilities of asking: by typing in an input box or by asking with the voice trough the use of Microphone.

## Tech Stack
Technologically, the app is crafted using the React Native framework, incorporating NativeWind for modern and responsive styling (TailwindCSS for RN). TypeScript is employed as a typed language, enhancing the application's robustness and maintainability.

The app is versatile and runs on both mobile platforms (Android and iOS). (Note: In the demo app, only the iOS platform has been tested due to time constraints).

The user voice input has been implemented using React Native Voice library.

The AI assistant is based on the openAI APIs.

**Client:** React Native, NativeWind, RN Voice, OpenAI API, Axios
## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/gjorgjiev.daniel/4ec1cd56-c966-40e5-a4c6-cb1a3ea60bbd.git
```

Go to the project directory

```bash
  cd <project directory>
```

Install dependencies

```bash
  npm install
```

Install pods: go to ios folder
```bash
  cd ios
```
Run the pod installation command
```bash
  pod install
```


## Run on mobile device

Run on native mobile device (ios simulator): In this project only ios has been installed for simplicity.


Open Xcode and project xcworkspace folder
```bash
  Open Xcode
  Open App.xcworkspace in folder in /ios/app
```
Run app on simulatore
```bash
  Select App target
  Select iPhone simulator
  hit Run
```
## Authors

- [Daniel Gjorgjiev](https://www.linkedin.com/in/daniel-gjorgjiev/)

